require 'byebug'

class Code
  attr_accessor :pegs

  def self.parse(string)
    str = string.chars.map(&:downcase)
    values = PEGS.values
    if string.class == String && str.all? {|peg| values.include?(peg) }
      Code.new(str)
    else
      raise "You Entered An Incorrect Peg String!!!"
    end
  end

  def initialize(pegs)
    @pegs = pegs
  end

  PEGS = {
    orange: "o",
    yellow: "y",
    blue: "b",
    red: "r",
    green: "g",
    purple: "p"
  }

  def [](num)
    self.pegs[num]
  end

  def == (num)
    if num.class == Code && self.pegs == num.pegs
      return true
    end
    false
  end


  def self.random
    random_str = ""
    ran = rand 6
    values = PEGS.values
    4.times {random_str += values[ran]}
    Code.new(random_str)
  end

  def exact_matches(pegs_2)
    matches = 0
    pegs_2.pegs.each_index do |idx|
      if pegs[idx] == pegs_2[idx]
        matches += 1
      end
    end
    matches
  end



  # def near_matches(pegs_2)
  #   counter = 0
  #   set_2 = pegs_2.colors
  #   self.colors.each do |peg, count|
  #     next unless set_2.has_key?(peg)
  #
  #     counter += [count, set_2[peg]].min
  #   end
  #   final = counter - self.exact_matches(pegs_2)
  #   final
  # end

  def near_matches(pegs_2)
    counter = 0
    PEGS.values.each do |peg|
      counter += [@pegs.count(peg), pegs_2.pegs.count(peg)].min
    end
    final = counter - self.exact_matches(pegs_2)
    final
  end


  def colors
    colors_hash = Hash.new(0)

    pegs.each do |peg|
      colors_hash[peg] += 1
    end
    colors_hash
    p colors_hash
  end
end

class Game
  attr_accessor :secret_code

  def initialize(secret_code= Code.random)
    @secret_code = secret_code
  end

  def get_guess
    input = gets.chomp
    Code.parse(input)
  end

  def display_matches(num)
    near = @secret_code.near_matches(num)
    exact = @secret_code.exact_matches(num)

    print "Your near matches are: #{near}"
    print "Your exact matches are: #{exact}"
  end

end
